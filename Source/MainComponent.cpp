/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    textButton1.setButtonText("Button 1");
    addAndMakeVisible(textButton1);
    textButton1.addListener(this);
    
    textButton2.setButtonText("Button 2");
    addAndMakeVisible(textButton2);
    textButton2.addListener(this);
    
    slider1.setSliderStyle(Slider::LinearHorizontal);
    addAndMakeVisible(slider1);
    slider1.addListener(this);
    
    comboBox1.addItem("Item 1", 1);
    comboBox1.addItem("Item 2", 2);
    comboBox1.addItem("Item 3", 3);
    addAndMakeVisible(comboBox1);
    
    textEditor1.setText("Text test");
    addAndMakeVisible(textEditor1);
    
    
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    DBG("Width = " << getWidth() << "\n");
    DBG("Height = " << getHeight());
    
    textButton1.setBounds(10, 10, getWidth() - 20, 40);
    slider1.setBounds(10, 50, getWidth() - 20, 40);
    comboBox1.setBounds(10, 90, getWidth() - 20, 40);
    textEditor1.setBounds(10, 140, getWidth() - 20, 40);
    textButton2.setBounds(10, 190, getWidth() - 20, 40);
}

void MainComponent::buttonClicked(Button* button)
{
    
    if (button == &textButton1)
        DBG ("Button 1 clicked\n");
    else if (button == &textButton2)
        DBG ("Button 2 clicked\n");
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    DBG ("Slider value = " << slider->getValue());
}
